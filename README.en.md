# ros_environment

#### Description

The package provides the environment variables `ROS_VERSION`, `ROS_DISTRO`, `ROS_PACKAGE_PATH`, and `ROS_ETC_DIR`.

#### Software Architecture
Software architecture description

ros_environment provides the environment variables

input:
```
ros_environment/
├── CMakeLists.txt
├── env-hooks
│   ├── 1.ros_distro.bat.in
│   ├── 1.ros_distro.sh.in
│   ├── 1.ros_etc_dir.bat.em
│   ├── 1.ros_etc_dir.sh.em
│   ├── 1.ros_package_path.bat.em
│   ├── 1.ros_package_path.sh.em
│   ├── 1.ros_python_version.bat.in
│   ├── 1.ros_python_version.sh.in
│   ├── 1.ros_version.bat.in
│   ├── 1.ros_version.sh.in
│   └── _parent_package_path.py.em
├── LICENSE
└── package.xml

```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-ros_environment/ros-noetic-ros-ros_environment-1.3.2-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-ros_environment/ros-noetic-ros-ros_environment-1.3.2-1.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-ros_environment-1.3.2-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-ros_environment-1.3.2-1.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

```
ros_environment/
├── cmake.lock
├── env.sh
├── etc
│   └── catkin
│       └── profile.d
├── lib
│   └── pkgconfig
│       └── ros_environment.pc
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── ros_environment
        └── cmake

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
