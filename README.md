# ros_environment

#### 介绍
ros_environment package 提供ROS常用的环境变量  `ROS_VERSION`, `ROS_DISTRO`, `ROS_PACKAGE_PATH`和 `ROS_ETC_DIR`.

#### 软件架构
软件架构说明

ros_environment  包含ROS常用的环境变量

文件内容:
```
ros_environment/
├── CMakeLists.txt
├── env-hooks
│   ├── 1.ros_distro.bat.in
│   ├── 1.ros_distro.sh.in
│   ├── 1.ros_etc_dir.bat.em
│   ├── 1.ros_etc_dir.sh.em
│   ├── 1.ros_package_path.bat.em
│   ├── 1.ros_package_path.sh.em
│   ├── 1.ros_python_version.bat.in
│   ├── 1.ros_python_version.sh.in
│   ├── 1.ros_version.bat.in
│   ├── 1.ros_version.sh.in
│   └── _parent_package_path.py.em
├── LICENSE
└── package.xml
```

#### 安装教程

1.  下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-ros_environment/ros-noetic-ros-ros_environment-1.3.2-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-ros_environment/ros-noetic-ros-ros_environment-1.3.2-1.oe2203.x86_64.rpm 

2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-ros_environment-1.3.2-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-ros_environment-1.3.2-1.oe2203.x86_64.rpm --nodeps --force

#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
ros_environment/
├── cmake.lock
├── env.sh
├── etc
│   └── catkin
│       └── profile.d
├── lib
│   └── pkgconfig
│       └── ros_environment.pc
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── ros_environment
        └── cmake
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
